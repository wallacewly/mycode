#! /usr/bin/python

import os
import re
from pprint import pprint
from typing import *
import pandas as pd

import GEOparse
from GEOparse import *
from GEOparse import utils
from functional import *

accession_name = "GSE57xxxx"

gse = cast(GSM, GEOparse.get_GEO(accession_name, destdir="/tmp"))

## save experiment design and sample information
exp = pd.DataFrame(gse.phenotype_data)
exp.to_csv(accession_name + "_expdesgin.csv")

## download SRA file
### set basic parameters

filetype = 'sra'
keep_sra = True
fastq_dump_options = {
	'skip-technical': None,
	'clip': None,
	'split-files': None,
	'readids': None,
	'read-filter': 'pass',
	'dumpbase': None,
	'gzip': None
	}
sra_kwargs = {
    "keep_sra": keep_sra,
    'filetype': filetype,
    "fastq_dump_options": fastq_dump_options
}
directory_path = "./" + accession_name
gse.download_supplementary_files(directory_path, True, "wallacewly@gmail.com", sra_kwargs)