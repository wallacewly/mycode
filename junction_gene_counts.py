#!/usr/bin/env python

#import subprocess as sp
import os, re, sys, glob, gzip, argparse
import pandas as pd
from collections import OrderedDict
import pysam

""" This script will count regional reads and junction reads for a give gene. 
    I modified some functions from ggsashimi.py
    Usage: python counts_4_sashimi.py -f $bamfolder -b $gtfile -g $igene -i $batch 
    """

def parse_gtf(gtfile, gene_name):
    ''' Parse gtf file and return dict for queried genes. 
    The with three keys, "gene","transcript","exon"
    '''
    chroms = [*range(1, 23), "X","Y"]
    chroms.extend(["chr"+str(x) for x in chroms])
    
    result = {}; result['exon'] = [] ; result['gene'] = []; result['transcript'] = []
    
    with gzip.open(gtfile, 'rt') if gtfile.endswith(".gz") else open(gtfile) as openf:
      for line in openf:
          if line.startswith("#"):
              continue
            
          if gene_name in line:
            chr, source, feature_type, start, end, score, strand, frame, gene_attr = line.strip().split("\t")
            
            if chr not in chroms:
                #print(f"{chr} is not recogized, it must be in {chroms}")
                continue
  
            gene_id_attr = gene_attr.split(';')
            gene_id_attr = [x.strip() for x in gene_id_attr[0:-1]]
            
            attr = {x.split()[0]:x.split()[1].strip().replace('"', "") for x in gene_id_attr }

            if attr.get('gene_name') == gene_name:
                if feature_type == 'gene':
                    ensembl_id = attr.get('gene_id')
                    result['gene'] = [gene_name, chr, int(start),  int(end), strand, ensembl_id]
                elif feature_type == 'transcript':
                    transcript_id = attr.get('transcript_id')
                    transcript_name = attr.get('transcript_name')
                    result['transcript'].append([transcript_id, chr,  int(start),  int(end), strand,transcript_name])
                elif feature_type == 'exon':
                    exon_id = attr.get('exon_id')
                    transcript_id = attr.get('transcript_id')
                    transcript_name = attr.get('transcript_name')
                    result['exon'].append([exon_id, chr,  int(start),  int(end), strand,transcript_name,transcript_id])
                  
    return result

def count_operator(CIGAR_op, CIGAR_len, pos, start, end, a, junctions):
        # Match
        if CIGAR_op == "M":
                for i in range(pos, pos + CIGAR_len):
                        if i < start or i >= end:
                                continue
                        ind = i - start
                        a[ind] += 1

        # Insertion or Soft-clip
        if CIGAR_op == "I" or CIGAR_op == "S":
                return pos
        # Deletion
        if CIGAR_op == "D":
                pass

        # Junction
        if CIGAR_op == "N":
                don = pos
                acc = pos + CIGAR_len
                if don > start and acc < end:
                        junctions[(don,acc)] = junctions.setdefault((don,acc), 0) + 1

        pos = pos + CIGAR_len

        return pos

def flip_read(s, samflag):
        if s == "NONE" or s == "SENSE":
                return 0
        if s == "ANTISENSE":
                return 1
        if s == "MATE1_SENSE":
                if int(samflag) & 64:
                        return 0
                if int(samflag) & 128:
                        return 1
        if s == "MATE2_SENSE":
                if int(samflag) & 64:
                        return 1
                if int(samflag) & 128:
                        return 0

def read_bam(f, chr, start, end, s):
        # Initialize coverage array and junction dict
        a = {"+" : [0] * (end - start)}
        junctions = {"+": OrderedDict()}
        if s != "NONE":
                a["-"] = [0] * (end - start)
                junctions["-"] = OrderedDict()

        samfile = pysam.AlignmentFile(f)

        for read in samfile.fetch(chr, start, end):
                # Move forward if read is unmapped
                if read.is_unmapped:
                    continue

                samflag, read_start, CIGAR = read.flag, read.reference_start+1, read.cigarstring

                # Ignore reads with more exotic CIGAR operators
                if any(map(lambda x: x in CIGAR, ["H", "P", "X", "="])):
                        continue

                read_strand = ["+", "-"][flip_read(s, samflag) ^ bool(int(samflag) & 16)]
                if s == "NONE": read_strand = "+"

                CIGAR_lens = re.split("[MIDNS]", CIGAR)[:-1]
                CIGAR_ops = re.split("[0-9]+", CIGAR)[1:]

                pos = read_start

                for n, CIGAR_op in enumerate(CIGAR_ops):
                        CIGAR_len = int(CIGAR_lens[n])
                        pos = count_operator(CIGAR_op, CIGAR_len, pos, start, end, a[read_strand], junctions[read_strand])

        samfile.close()
        return a, junctions


if __name__ == "__main__":
  
    parser = argparse.ArgumentParser(description='Count read depth and junction reads for a given gene', add_help=True)
    # parser.register('action', 'debuginfo', DebugInfoAction)
    parser.add_argument("-f", "--bam-folder", dest="bamfolder", type=str, required=True, help="The folder that contains bam files")
    parser.add_argument("-b", "--gtf", type=str,dest="gtf", required=True, help="GTF file")
    parser.add_argument("-g", "--gene", type=str,dest="gene", required=True, help="gene symbol")
    parser.add_argument("-i", "--ibatch",dest="batch", type=str, help="only include bam files that contains certain string, e.g. 'PR8' for scHHD")
    parser.add_argument("-o", "--out-prefix", dest="outfile", type=str, help="test")
    
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()
    
    ofolder="/hpc/group/kolab/lw157/spliceQTLs/scHHD7batch_bigwig/"
    #gtfile="/hpc/group/kolab/lw157/spliceQTLs/ref/gencode.v32_hg38.annotation.gtf.gz"
    #igene = "PARP2"
    #ibat = "PR8"
    #bamfiles = glob.glob(f"/hpc/group/kolab/scHH-diversity/LCLBams/*{ibat}/*{ibat}.bam")
    
    gtfile= args.gtf
    igene = args.gene
    ibat = args.batch
    bamfolder=args.bamfolder
    bamfiles = glob.glob(f"{bamfolder}/*{ibat}/*{ibat}.bam")

    print(len(bamfiles), f"samples will be loading from scHHD batch {ibat}")
    
    gene_info = parse_gtf(gtfile, igene)
    if len(gene_info["gene"]) < 1:
        print(f"{igene} is not in {gtfile} file")
        sys.exit(1)
    igene_name, ichr, igene_start, igene_end, istrand, igene_id = gene_info['gene']
    istrand = "NONE" ## Here I set "strand" to NONE, as I would count reads for both strands
    print(f"Counting reads for gene {igene_name}, ranging from {igene_start} to {igene_end} on {ichr}")
    if args.outfile:
        o_prefix = args.outfile
    else:
        o_prefix = f"{ofolder}/{igene_name}_{ichr}_{igene_start}_{igene_end}_scHHD{ibat}"
        
    junc_out = open(f"{o_prefix}_junction_counts.txt", 'w')
    junc_out.write("sample\tjunc_start\tjunc_end\tcounts\n")
    
    cnts_dict = {}; junc_dict = {}
    
    for ibam in bamfiles:
      if not os.path.isfile(ibam):
          continue
      id = os.path.basename(ibam).split("_")[0]
      exon_cnt, junc_cnt = read_bam(ibam, ichr, igene_start, igene_end, istrand)
    
      if len(exon_cnt.keys()) > 0:
        cnts_dict[id] = list(exon_cnt.values())[0]
        
      if len(junc_cnt.keys() ) > 0:
        junc_dict[id] = junc_cnt["+"].items() 
    
    for id, k in junc_dict.items():
      for (j_start, j_end), junc_c in k:
        junc_out.write(f"{id}\t{j_start}\t{j_end}\t{junc_c}\n")
    junc_out.close()
    
    readpt = pd.DataFrame.from_dict(cnts_dict, orient="columns").reset_index()
    readpt.index =  range(igene_start, igene_end )
    readpt.to_csv(f"{o_prefix}_gene_counts.txt.gz", sep='\t')
    
    print("Done!")

